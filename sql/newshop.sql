-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 27, 2021 at 11:41 AM
-- Server version: 5.7.31
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `profile_image` varchar(200) DEFAULT NULL,
  `user_type` tinyint(4) DEFAULT NULL COMMENT '1=>admin 2=>shopper',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=>inactive 1=>active ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `password`, `display_name`, `profile_image`, `user_type`, `status`) VALUES
(1, 'TestShopper', '202cb962ac59075b964b07152d234b70', 'Super Admin', 'assets/uploads/services/1585048698_download_(4).jfif', 2, 1),
(2, 'Admin', 'e10adc3949ba59abbe56e057f20f883e', 'Super Admin', 'assets/uploads/services/1566380205_163235.jpg', 1, 1),
(3, 'admin', '202cb962ac59075b964b07152d234b70', 'shopperrrr', 'assets/uploads/services/1566205885_Hydrangeas.jpg', 2, 1),
(4, 'admin', '202cb962ac59075b964b07152d234b70', 'shopperrrr', 'assets/uploads/services/1566206000_Hydrangeas.jpg', 2, 1),
(5, 'admin', '202cb962ac59075b964b07152d234b70', 'shopperrrr', 'assets/uploads/services/1566206154_Hydrangeas.jpg', 2, 1),
(6, 'admin', '202cb962ac59075b964b07152d234b70', 'new shoppee', 'assets/uploads/services/1566272793_Hydrangeas.jpg', 2, 1),
(7, 'admin', '202cb962ac59075b964b07152d234b70', 'new shoppee', 'assets/uploads/services/1566272882_Hydrangeas.jpg', 2, 2),
(8, 'vyshna', '202cb962ac59075b964b07152d234b70', 'shopper1', 'assets/uploads/services/1566273432_gettyimages-139496979-612x612.jpg', 2, 1),
(9, 'vyshna', '202cb962ac59075b964b07152d234b70', 'shopper1', 'assets/uploads/services/1566273491_gettyimages-139496979-612x612.jpg', 2, 1),
(10, 'vyshna', '202cb962ac59075b964b07152d234b70', 'shopper1', 'assets/uploads/services/1566273537_Hydrangeas.jpg', 2, 1),
(11, 'vyshna', '202cb962ac59075b964b07152d234b70', 'shopper1', 'assets/uploads/services/1566273666_Hydrangeas.jpg', 2, 1),
(12, 'vyshna', '202cb962ac59075b964b07152d234b70', 'shopper1', 'assets/uploads/services/1566273707_Hydrangeas.jpg', 2, 1),
(13, 'vyshna', '202cb962ac59075b964b07152d234b70', 'shopper1', 'assets/uploads/services/1566273734_Hydrangeas.jpg', 2, 1),
(14, 'vyshnam', '202cb962ac59075b964b07152d234b70', 'shopper1', 'assets/uploads/services/1566274028_Hydrangeas.jpg', 2, 1),
(15, 'vyyvy', '202cb962ac59075b964b07152d234b70', 'shopper1', 'assets/uploads/services/1566274200_Hydrangeas.jpg', 2, 1),
(16, 'new shopper', '202cb962ac59075b964b07152d234b70', 'shopper1', 'assets/uploads/services/1566274255_Hydrangeas.jpg', 2, 1),
(17, 'new shopppee', '202cb962ac59075b964b07152d234b70', 'shopper1', 'assets/uploads/services/1566274303_Hydrangeas.jpg', 2, 2),
(18, 'shopper1', '202cb962ac59075b964b07152d234b70', 'vyshhshhs', 'assets/uploads/services/1566462958_depositphotos_80441828-stock-illustration-shopkeeper-vector-design.jpg', 2, 1),
(19, 'shoppy', '202cb962ac59075b964b07152d234b70', 'shoppyyyy', 'assets/uploads/services/1566462266_depositphotos_80441828-stock-illustration-shopkeeper-vector-design.jpg', 2, 2),
(20, 'adminbdhb', '202cb962ac59075b964b07152d234b70', 'ssss', 'assets/uploads/services/1568284104_depositphotos_80441828-stock-illustration-shopkeeper-vector-design.jpg', 2, 1),
(21, 'ShopperV', '25f9e794323b453885f5181f1b624d0b', 'Shopper Vy', 'assets/uploads/services/1581316737_1f601.png', 2, 2),
(22, 'testshopper', '25d55ad283aa400af464c76d713c07ad', 'test shopper', 'assets/uploads/services/1583150180_PP.jpg', 2, 2),
(23, 'manju', '25d55ad283aa400af464c76d713c07ad', 'manju', 'assets/uploads/services/1583401295_13.jpg', 2, 2),
(24, 'appu', '25d55ad283aa400af464c76d713c07ad', 'appu', 'assets/uploads/services/1583401437_11.jpg', 2, 0),
(25, 'manu', '25d55ad283aa400af464c76d713c07ad', 'manu', 'assets/uploads/services/1583407204_4.jpg', 2, 0),
(26, 'honey', '25d55ad283aa400af464c76d713c07ad', 'honey', 'assets/uploads/services/1583826306_5.jpg', 2, 1),
(27, 'laya', '25d55ad283aa400af464c76d713c07ad', 'laya', 'assets/uploads/services/1584011847_14.jpg', 2, 0),
(28, 'jose', '25d55ad283aa400af464c76d713c07ad', 'jose', 'assets/uploads/services/1584012201_mae-mu-U1iYwZ8Dx7k-unsplash.jpg', 2, 1),
(29, 'sdfghjkl', '202cb962ac59075b964b07152d234b70', '6vuyyyyyy', 'assets/uploads/services/1584077137_download_(7).jfif', 2, 0),
(30, 'vyshna test', 'e10adc3949ba59abbe56e057f20f883e', 'vyshna test', 'assets/uploads/services/1584421839_images_(19).jfif', 2, 1),
(31, 'mahi', '25d55ad283aa400af464c76d713c07ad', 'mahi', 'assets/uploads/services/1585061390_14.jpg', 2, 0),
(32, 'jiji', '25d55ad283aa400af464c76d713c07ad', 'jiji', 'assets/uploads/services/1585289523_brooke-lark-3A1etBW5cBk-unsplash.jpg', 2, 1),
(33, 'Owner', '25d55ad283aa400af464c76d713c07ad', 'Nfbd', 'assets/uploads/services/1586083597_IMG_20200405_152950612.jpg', 2, 1),
(34, 'akil9399', 'e10adc3949ba59abbe56e057f20f883e', 'LaximiStore', 'assets/uploads/services/1586107473_81NlKTZCBiL._SX679_-300x300.jpg', 2, 1),
(35, 'RoyaleKirana', 'e10adc3949ba59abbe56e057f20f883e', 'Royale', 'assets/uploads/services/1586107551_1203740_1-surf-excel-liquid-detergent-matic-top-load-500ml-matic-top-load-detergent-powder-2-kg.jpg', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_table`
--

DROP TABLE IF EXISTS `auth_table`;
CREATE TABLE IF NOT EXISTS `auth_table` (
  `auth_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `unique_id` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`auth_id`)
) ENGINE=MyISAM AUTO_INCREMENT=179 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_table`
--

INSERT INTO `auth_table` (`auth_id`, `user_id`, `unique_id`, `status`) VALUES
(9, 1, '6cef882d57f88b2cd817b1f017533927', 1),
(8, 1, 'b09b95a8a320da312032433eb89afa54', 0),
(7, 1, '53253f37afbf5ae378b58aac800ca982', 0),
(10, 1, '2231d58592717bfedf0990898d8c0ac7', 1),
(11, 2, '6ac68295877a7dc432d77c763040c269', 1),
(12, 2, 'd8c986811f27752cab2a7eac4d753d3b', 1),
(14, 2, '07dfb686a03668a08e672448bfa1b3a7', 1),
(15, 2, 'c3f8d3ace1dd98cfdc77c156f4e36556', 1),
(16, 2, '03caa8c5b418cac7f8fa217601c7f6fe', 1),
(17, 2, '1373ef0f71e01edb29cc568f0c6e9369', 1),
(18, 9, '8fc5f3e5648813a7c17250f01263806e', 1),
(19, 10, 'a02b4d10f9f0c5c7cc5a7515e058bef6', 1),
(20, 2, '1067567900083cb0497c0e045f6bf76f', 1),
(21, 2, '1e4661987b210ddf88a9bbbe9514471a', 1),
(22, 11, 'c337265ffd48740eae10721464dd889a', 1),
(23, NULL, 'e287fbe7e3b137f11def1f87a518b288', 1),
(24, NULL, '009c2f3842736febd593a1e13bebb991', 1),
(25, 18, '8d37caa74b3bfe67549c4f11557f6a86', 1),
(26, 20, '5e18201c11a56231c9a2e14fc45ae820', 1),
(27, 21, 'df7996cb6b263c1b8a4d126ad27d9124', 1),
(28, 22, 'e69f7decc9b71e78e7b46b74cd90cf9b', 1),
(29, 23, '8d4be73e92645e25d859c27b07d2a6b3', 1),
(30, 29, '16d588300168f8addb78418f17520b78', 1),
(31, 30, '089f6630139000679b4369356c100c77', 0),
(32, 1, '5cb685677bd31f3a206c46cd7019fffb', 1),
(33, 31, '9b23a108cfc3944d0ab9128dc7055957', 1),
(34, 1, '5b181ed05c31ad31608a6fa29c3b94f0', 1),
(35, 32, '39e79fb95b9b42b63d47fc7021230b18', 1),
(36, 31, 'da36b5600be9d08f78ba6897478774ef', 1),
(37, 33, 'd00ed6b41a38ee4eba861bef763e5b52', 1),
(38, 31, '3d7a4f2edf1b111028ad9e88afce4702', 1),
(39, 31, '90cb88b342db020a6273c8800d0b130f', 1),
(40, 34, 'c1724326f4fb0156383b760f0930a76a', 1),
(41, 34, '5fe15534351f60f0e91e65101c166c6c', 1),
(42, 34, '42453d21e125f4e0c026081e96084493', 1),
(43, 34, 'ee3d743813a3afc6e6fe82fa8598adbc', 1),
(44, 34, '6ae2a2c0315afe754d9e9c30a7a90c53', 1),
(45, 34, '62e8725ca7e98e08488b22b93ee3e3fc', 1),
(46, 34, '7751fcac31e4922e090619413d79fa40', 1),
(47, 34, '5b9299cf99a322dce53f1500d5e38b84', 1),
(48, 34, 'beb5811e6778e0de7ebfbab457dffa06', 1),
(49, 34, 'a6178b01b4650d647f35576d0b2476a6', 1),
(50, 35, '485df1aadf0fbdf0ab5c48a95f34a364', 1),
(51, 34, '98da109918c63c0dc247ca9ee533819e', 1),
(52, 34, 'e4d3ee7b6ea764b3779ed657b6c4b6e9', 1),
(53, 34, '8bed803d27b24f67a369de899688aead', 1),
(54, 34, '6e128f6a2dcbfa454296746b9eaf0836', 1),
(55, 34, '30103ee1c247b0071f0c00e536b0f84e', 1),
(56, 34, '9ea2ba7ce3a15b2f9845f8676909a4ee', 1),
(57, 34, '5d370b7c83be479359e4feabfd9729da', 1),
(58, 34, '544bf81966ba148a1d0d911df8fb59bb', 1),
(59, 34, '2b190b3e38724106c5c6fe0ae63b4959', 1),
(60, 34, 'df19024bab54d65aecfccc35e07bf958', 1),
(61, 34, '4988f93e9e8df52d0750fb26bc1cc697', 1),
(62, 34, '6e69d7c1f3e68daf10ccab7d39d0d472', 1),
(63, 34, 'fd6bde8f41f6e4355ef360e25c9e5a68', 1),
(64, 34, '322e709de8d83700aa118757c45b6f2a', 1),
(65, 34, '5985096bd5147bb400b46151b9afc576', 1),
(66, 34, '9118d995b208254fbe41e21cab7f34da', 1),
(67, 34, 'e4620b963c45249d055f7c26ab6fa60f', 1),
(68, 36, 'Clickkart071d31979dd0d1009bf9ca0e9a5be3265dc1919e', 1),
(69, 37, '384fbfc3bf5aefe70b24765ba714521b', 1),
(70, 38, '3981d4b688d237c8c1cd7733096ceb24', 1),
(71, 39, '791034a00bbf23887a78dc1ec4177877', 1),
(72, 40, 'd372d1aebe113fdad8e8c2912e5b0486', 1),
(73, 46, 'Clickkartf3963eb427b0d220f768d0a47cf76cad138931bd', 1),
(74, 47, 'Clickkartb4311b40b23f833d8aacf10110cd3e3be06359b6', 1),
(75, 48, 'Clickkart642ca00f962ac0f8cacea4b778541e617e829eeb', 1),
(76, 49, 'Clickkart7cac13598c44796798450d7d56524ab2fadc62b5', 1),
(77, 50, 'Clickkart444efb0fb78084723e49d07395190f2667353dc3', 1),
(78, 51, 'Clickkart5ec9197625abc4dd9f2c370c2fb6e2deab1028d5', 1),
(79, 52, 'Clickkart300ac9c13a0f9b431d42d7eb4c483b17fb1e35af', 1),
(80, 53, 'Clickkart60490d0e8155da840188790ec2558b02ccf1391b', 1),
(81, 54, 'Clickkarta03fa5d9aeec91a29d54eb480d7a8348ff4c00ff', 1),
(82, 55, 'Clickkartdca6864868926b90143258093a07e43be96bc563', 1),
(83, 57, 'Clickkart1f1179d382db902f14c4239f1bea2e4cf78d562d', 1),
(84, 58, 'Clickkart3b92cf781a8e142fdc053a9cea13722a5e28a713', 1),
(85, 59, 'Clickkart4ad80eb0e95bfd5a253e6ed1095e3f3b1a627dd8', 1),
(86, 56, 'Clickkart8280fd00a2cc1efeaaaff04cc665153286db38d8', 1),
(87, 60, 'Clickkartf55eb82e9e0132842c277c0faf8f0a2137a4d87a', 1),
(88, 61, 'Clickkartffa83c4cd370aa552df88d3bbf6d07a3cd2f69d2', 1),
(89, 62, 'Clickkartabdb7440ca9cb9d705cb1fffa12a9a0176c09cd9', 1),
(90, 64, 'Clickkart51b2eda8db0e9026f8e217391f9806cc7e453779', 1),
(91, 65, 'Clickkartb0290dd8ed4f20c683da31741909241a5fdbbc4e', 1),
(92, 66, 'Clickkart4201b7df9862bd7fdafc0d4611c60acb3d440951', 1),
(93, 67, 'Clickkartf3080a65e89b71b8859c5a2616a68083616f9651', 1),
(94, 69, 'Clickkartf23f1602805c807723cf23551e814bbbd5182134', 1),
(95, 69, 'Clickkartf23f1602805c807723cf23551e814bbbd5182134', 1),
(96, 69, 'Clickkartf23f1602805c807723cf23551e814bbbd5182134', 1),
(97, 69, 'Clickkartf23f1602805c807723cf23551e814bbbd5182134', 1),
(98, 73, 'Clickkart6b7e23af159451281c8389518da5e69e8d1602c9', 1),
(99, 74, 'Clickkartb7f4a16e4b3b740982eba88c1f38ed0e3e09e892', 1),
(100, 75, 'Clickkart7e6e8bd844560868c80ae8c5d7e20c5d338e2915', 1),
(101, 76, 'Clickkarta889f849b05b6d834fadec4aecae518f6b00ecea', 1),
(102, 68, 'Clickkarte7e5f0bd54e6ad74ff7b0c3775bf439c6ccb4931', 1),
(103, 77, 'Clickkart1b6f5d2cd26e4674197c3f7737cce4f76c4bec32', 1),
(104, 78, 'Clickkarta43adf965d68bb5fe9224fd101628952a3f2a106', 1),
(105, 79, 'Clickkarte821ffef73e4cae2b51e9894b97edbe74d8a150b', 1),
(106, 80, 'Clickkart78fe4d379cb0b6dfb18f4fc93d89856f5465199d', 1),
(107, 81, 'Clickkart7ac96571d96cb5f4a0f8e14435808f64ff8be196', 1),
(108, 82, 'Clickkartf04e51ea3dccb2943d5d21b79689903f84ff8989', 1),
(109, 83, 'Clickkart613b12a857f72374b2517c2a7c540f21d3697793', 1),
(110, 84, 'Clickkartc3266dbbca8a0ef23d8fbf0702fedc893d25448b', 1),
(111, 85, 'Clickkartf792c76ba52012d345074da188aba851faa7ceaf', 1),
(112, 86, 'Clickkartc61d562e4672dce32c15c5330bf246dff02f0768', 1),
(113, 87, 'Clickkart19ea327eb355bdedfe1c8162a987d84da97e7f35', 1),
(114, 88, 'Clickkartb930fd4b96983d81187dc4fb3427f813ea018e52', 1),
(115, 89, 'Clickkarte02db1cb7082b495a36556c2bc1b80de85cf7069', 1),
(116, 90, 'Clickkart22de6f694a8f7ba1ff8191cce39bb344d98fcaa8', 1),
(117, 91, 'Clickkart1b6947bd22b638ff4f073c43f40796bc7b782a5d', 1),
(118, 92, 'Clickkartd983ad9fa02cae3832c1b341e4b2fd5569793b05', 1),
(119, 93, 'Clickkart092da2c65bcbe7fc8eb91afe3dc74a9615599b6f', 1),
(120, 94, 'Clickkartd8d4181f4424bbbca3f2ff86c2bf2744b122c774', 1),
(121, 95, 'Clickkartc29680388081abac376d2938cf232a450d3091a8', 1),
(122, 96, 'Clickkart534665a30500a23f77433158333a0e0bddf20fb5', 1),
(123, 97, 'Clickkart4a77887f780501335b7d03866bf8c8a2f84e5ffb', 1),
(124, 98, 'Clickkartc9a48eb7fd2e96372add8319c57fce602d65b3d7', 1),
(125, 99, 'Clickkart9a3bbb94b5b070bcecef1377dc49b4f4fe0dfb12', 1),
(126, 100, 'Clickkart2578d23c6d4eba4fd0f6ad6cf42d607c0569cb92', 1),
(127, 101, 'Clickkartc52f8d5d95d45d76c49a297744fdf97c3316ade9', 1),
(128, 102, 'Clickkartb98c94b8852a725a1c31137c88a5c2e330d1f6ae', 1),
(129, 103, 'Clickkart366bbdd1d13d079c476b36e9590cbc46afd43383', 1),
(130, 104, 'Clickkartc325595228b78d4697af645649b2179c4e7b8f75', 1),
(131, 105, 'Clickkart7a21e48f06efa7ea301b27f73c67c4fd45b027b2', 1),
(132, 106, 'Clickkart288884b6d0f64637712138d8a7819a8f0bc4dc97', 1),
(133, 107, 'Clickkarte74280a8d7e78cc4d218967bbc5f6874917489d4', 1),
(134, 108, 'Clickkart2bf86b7c9904c3544fa240fc6e3f8c3c464f1427', 1),
(135, 109, 'Clickkartb5c75590832c103a132d6a8e7896322a6db58e2d', 1),
(136, 110, 'Clickkartab7b6ac553e4acc35abdeaa544415fa5298764c6', 1),
(137, 111, 'Clickkart54f1499ca20565e016c3ef80da9b8cf726f0b9d6', 1),
(138, 112, 'Clickkart35c9ed140aa32823e636e061109855fce24a3197', 1),
(139, 113, 'Clickkartc53409cc59e13db366f3e92754b3d880f4053843', 1),
(140, 114, 'Clickkartd5ac647965fb209a88edc10192ad65c1e943b719', 1),
(141, 115, 'Clickkarta94b077b52ebad4ef413e8c9fc5be7d6ea20533f', 1),
(142, 116, 'Clickkart61c795bed2ad69d8e0a9550ac72d42f64bbda0ce', 1),
(143, 116, 'Clickkart61c795bed2ad69d8e0a9550ac72d42f64bbda0ce', 1),
(144, 118, 'Clickkart31979de5f21306798e0b25279ad4f0461ec4aba8', 1),
(145, 119, 'Clickkartc9980ab56e1f2ef3bd15fd45fcd32e5a321ccfad', 1),
(146, 120, 'Clickkartafa07b99118c1c6953dbe926318a172220367323', 1),
(147, 121, 'Clickkart3a9eeab1fb6e6250266c562095045ce7b5492ca7', 1),
(148, 122, 'Clickkartcc13b54913931ef89ac5ccc24b6f9c428b5604c9', 1),
(149, 123, 'Clickkart977d63b8ab8e27d9a4b844522784b8ca8695c1cd', 1),
(150, 124, 'Clickkartb8224372d54dafa5a9522da64ae16873b9e7a8f5', 1),
(151, 125, 'Clickkart2d676d92a7f412698daf5bdf6adbff589cfebf8b', 1),
(152, 126, 'Clickkartd68652b4409996ddedb0ffc651af126fce1f285c', 1),
(153, 127, 'Clickkartd69a3cf71c642c797f00d95253377e28c233b1cb', 1),
(154, 128, 'Clickkarta0ec6795e50c9b7193cd7514c94352a36d652b23', 1),
(155, 129, 'Clickkartfb5d3a0ae2087549c1ced906d0d47bfdb32dde3d', 1),
(156, 130, 'Clickkart5f828961914e4d5207479eabfbecb14926173a9f', 1),
(157, 131, 'Clickkart151359b5a77b70ae418c4406fb68ed33f7382ff0', 1),
(158, 132, 'Clickkart4dee99a180a81b85a1898dbee5c05593da87ca6c', 1),
(159, 133, 'Clickkartc64175259b02c4c76b5a7f406dedd5773d2f5943', 1),
(160, 134, 'Clickkart9ca943edf45f91c69f81c8c1ca92b0fed2002a7e', 1),
(161, 135, 'Clickkarte598d5577d3cbd22cefee6eedae6ae7ef444e52d', 1),
(162, 136, 'Clickkartf8a0968af13f73b55d336074fbfcc127a7254ecb', 1),
(163, 137, 'Clickkart2dedf6c6a8c8455d0d3bcc8fed6382fed82355b3', 1),
(164, 138, 'Clickkart3cf102306f58b5b37304b71294243a08581c7bdd', 1),
(165, 139, 'Clickkartffe13163daeb1f687c6ef168751ff50a59601fbe', 1),
(166, 140, 'Clickkarteb1dbe6b3e69faf45fcd11762c367416e3791ab2', 1),
(167, 141, 'Clickkartdcbfce5d68302c92529fd405dccad88f9ad504c7', 1),
(168, 142, 'Clickkart5a4c2e63c068fddedca635fb8b2bea586a465851', 1),
(169, 143, 'Clickkarta3bb66005c6af531e59f7e6b0b8b2b5d34eac0a8', 1),
(170, 144, 'Clickkartcd24d0c36bdb0fc568a9d5e4773f24af04ad1b3d', 1),
(171, 145, 'Clickkart7fb65fb3cda1403b8d5e1d463c3ccfbbb7f6f6c8', 1),
(172, 146, 'Clickkart21e74247166e23fdbb47d823760f27f7977c101e', 1),
(173, 147, 'Clickkartda8d4c8af0224ce6e2565290091ba7c16746ed01', 1),
(174, 148, 'Clickkart796ae4bf2c464ae07df140252e219251ec95c903', 1),
(175, 149, 'Clickkart538c2f387c3f0d6adf96f6bec14c1d2b60d55544', 1),
(176, 150, 'Clickkart27d9a5c4e9016500cb718df60c829d4c9719b9ea', 1),
(177, 151, 'Clickkarta2831966eb838b2ca49a92d97b8eed251046adbf', 1),
(178, 152, 'Clickkartd904dfaed82c5371260b694e7f86b892ca07e013', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=MyISAM AUTO_INCREMENT=190 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `user_id`, `store_id`, `product_id`, `quantity`, `status`) VALUES
(2, 23, NULL, 4, 6, 2),
(8, 23, NULL, 10, 6, 1),
(24, 46, 8, 58, 3, 2),
(25, 47, 1, 3, 2, 1),
(138, 90, 6, 54, 3, 1),
(177, 134, 7, 60, 1, 1),
(137, 85, 7, 76, 1, 1),
(59, 51, 1, 79, 3, 1),
(176, 134, 7, 59, 1, 1),
(131, 80, 1, 7, 1, 1),
(139, 90, 36, 117, 1, 1),
(130, 80, 1, 57, 1, 1),
(116, 56, 1, 57, 7, 1),
(111, 56, 26, 102, 3, 1),
(178, 134, 1, 4, 1, 1),
(154, 91, 1, 7, 1, 1),
(153, 86, 1, 57, 1, 1),
(152, 86, 1, 3, 1, 1),
(168, 115, 1, 1, 1, 1),
(175, 134, 7, 91, 1, 1),
(163, 89, 7, 59, 1, 1),
(162, 89, 1, 79, 1, 1),
(161, 89, 7, 91, 1, 1),
(179, 134, 1, 7, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) DEFAULT NULL,
  `category_image` varchar(200) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_image`, `status`) VALUES
(60, 'vegetables', 'assets/uploads/services/1614405790_abc.jpeg', 1),
(61, 'sweets', 'assets/uploads/services/1614405953_doughnuts.jpeg', 1),
(62, 'fruit', 'assets/uploads/services/1614407699_blueberries.jpeg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `booking_id` varchar(100) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `total_amount` int(100) DEFAULT NULL,
  `payment_method` tinyint(5) DEFAULT NULL COMMENT '1=>COD,2=>Online Payment',
  `payment_status` int(11) DEFAULT NULL COMMENT '1=>pending,2=>paid',
  `status` tinyint(4) DEFAULT NULL COMMENT '1=>ordered 2=>delivered 3=>packed 4=>shipped',
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=273 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `booking_id`, `product_id`, `category_id`, `address_id`, `total_amount`, `payment_method`, `payment_status`, `status`) VALUES
(260, 29, 'CK884820200408', 88, 61, 38, 350, 1, 1, 4),
(264, NULL, 'SH20210227', 5, 61, NULL, NULL, 1, 1, 2),
(272, NULL, 'SH20210227', 6, 62, NULL, 30, 1, 1, 1),
(271, NULL, 'SH20210227', 3, 60, NULL, 90, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

DROP TABLE IF EXISTS `order_product`;
CREATE TABLE IF NOT EXISTS `order_product` (
  `op_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` varchar(100) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`op_id`)
) ENGINE=MyISAM AUTO_INCREMENT=209 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`op_id`, `booking_id`, `product_id`, `quantity`, `price`, `status`) VALUES
(1, '1', 2, 2, 18, 1),
(2, '2', 7, 3, 18, 1),
(3, '3', 4, 2, 24, 1),
(4, '4', 10, 2, 22, 1),
(5, '5', 2, 5, 90, 1),
(6, '6', 5, 5, 60, 1),
(7, '0', 2, 1, NULL, 1),
(8, '0', 2, 3, NULL, 1),
(9, '0', 2, 1, NULL, 1),
(10, '0', 2, 3, NULL, 1),
(11, '0', 2, 1, NULL, 1),
(12, '0', 2, 3, NULL, 1),
(13, '0', 2, 1, NULL, 1),
(14, '0', 2, 3, NULL, 1),
(15, '0', 2, 1, NULL, 1),
(16, '0', 2, 3, NULL, 1),
(17, '0', 2, 1, NULL, 1),
(18, '0', 2, 3, NULL, 1),
(19, '0', 2, 1, NULL, 1),
(20, '0', 2, 3, NULL, 1),
(21, '0', 2, 1, NULL, 1),
(22, '0', 2, 3, NULL, 1),
(23, '0', 2, 1, NULL, 1),
(24, '0', 2, 3, NULL, 1),
(25, '0', 2, 1, NULL, 1),
(26, '0', 2, 3, NULL, 1),
(27, '0', 2, 1, NULL, 1),
(28, '0', 2, 3, NULL, 1),
(29, '0', 2, 1, NULL, 1),
(30, '0', 2, 3, NULL, 1),
(31, '0', 2, 1, NULL, 1),
(32, '0', 2, 3, NULL, 1),
(33, '0', 2, 1, NULL, 1),
(34, '0', 2, 3, NULL, 1),
(35, '0', 2, 1, NULL, 1),
(36, '0', 2, 3, NULL, 1),
(37, '0', 2, 1, NULL, 1),
(38, '0', 2, 3, NULL, 1),
(39, '0', 2, 1, NULL, 1),
(40, '0', 2, 3, NULL, 1),
(41, 'OrdID989120200203', 2, 1, NULL, 1),
(42, 'OrdID989120200203', 2, 3, NULL, 1),
(43, 'OrdID451220200203', 2, 1, 50, 1),
(44, 'OrdID451220200203', 2, 3, 50, 1),
(45, 'OrdID202120200203', 2, 1, 50, 1),
(46, 'OrdID202120200203', 2, 3, 50, 1),
(47, 'OrdID194320200203', 2, 1, 50, 1),
(48, 'OrdID194320200203', 2, 3, 50, 1),
(49, 'OrdID843820200203', 2, 1, 50, 1),
(50, 'OrdID843820200203', 2, 3, 50, 1),
(51, 'OrdID548520200203', 2, 1, 50, 1),
(52, 'OrdID548520200203', 2, 3, 50, 1),
(53, 'OrdID247020200204', 2, 1, 50, 1),
(54, 'OrdID247020200204', 2, 3, 50, 1),
(55, 'OrdID718620200204', 2, 1, 50, 1),
(56, 'OrdID718620200204', 2, 3, 50, 1),
(57, 'OrdID408120200204', 2, 1, 50, 1),
(58, 'OrdID408120200204', 2, 3, 50, 1),
(59, 'CK237020200204', 2, 1, 50, 1),
(60, 'CK237020200204', 2, 3, 50, 1),
(61, 'CK754420200206', 2, 1, 50, 1),
(62, 'CK754420200206', 2, 3, 50, 1),
(63, 'CK655820200206', 2, 1, 50, 1),
(64, 'CK655820200206', 2, 3, 50, 1),
(65, 'CK322920200207', 2, 1, 50, 1),
(66, 'CK322920200207', 2, 3, 50, 1),
(67, 'OrdID935220200207', 2, 1, 50, 1),
(68, 'OrdID935220200207', 2, 3, 50, 1),
(69, 'CK913020200207', 2, 1, 50, 1),
(70, 'CK913020200207', 2, 3, 50, 1),
(71, 'CK200520200207', 2, 1, 50, 1),
(72, 'CK200520200207', 2, 3, 50, 1),
(73, 'CK548420200207', 2, 1, 50, 1),
(74, 'CK548420200207', 2, 3, 50, 1),
(75, 'CK869120200207', 2, 1, 50, 1),
(76, 'CK869120200207', 2, 3, 50, 1),
(77, 'CK358820200310', 2, 1, 450, 1),
(78, 'CK358820200310', 2, 1, 2000, 1),
(79, 'CK484520200310', 2, 1, 300, 1),
(80, 'CK502020200311', 2, 1, 650, 1),
(81, 'CK502020200311', 2, 1, 2000, 1),
(82, 'CK567320200311', 2, 2, 24, 1),
(83, 'CK575720200311', 2, 1, 250, 1),
(84, 'CK575720200311', 2, 1, 120, 1),
(85, 'CK115020200311', 2, 2, 500, 1),
(86, 'CK582020200311', 2, 3, 250, 1),
(87, 'CK293020200311', 2, 1, 250, 1),
(88, 'CK988020200311', 2, 1, 120, 1),
(89, 'CK849620200311', 2, 2, 2000, 1),
(90, 'CK057420200311', 2, 1, 500, 1),
(91, 'CK524020200311', 2, 2, 450, 1),
(92, 'CK776720200311', 2, 3, 150, 1),
(93, 'CK193820200311', 2, 1, 150, 1),
(94, 'CK193820200311', 2, 1, 150, 1),
(95, 'CK193820200311', 2, 1, 450, 1),
(96, 'CK632020200311', 2, 4, 650, 1),
(97, 'CK999020200311', 2, 5, 120, 1),
(98, 'CK559220200311', 2, 6, 250, 1),
(99, 'CK992120200311', 2, 2, 300, 1),
(100, 'CK876020200311', 2, 2, 340, 1),
(101, 'CK420320200311', 2, 2, 340, 1),
(102, 'CK386120200311', 2, 2, 35, 1),
(103, 'CK446920200311', 2, 2, 650, 1),
(104, 'CK964020200311', 2, 2, 150, 1),
(105, 'CK158620200312', 2, 1, 18, 1),
(106, 'CK893720200313', 2, 1, 820, 1),
(107, 'CK420920200313', 2, 1, 6500, 1),
(108, 'CK826120200324', 2, 1, 280, 1),
(109, 'CK521020200324', 2, 1, 150, 1),
(110, 'CK521020200324', 2, 1, 100, 1),
(111, 'CK445220200325', 2, 3, 150, 1),
(112, 'CK456120200325', 2, 1, 280, 1),
(113, 'CK311220200325', 2, 1, 280, 1),
(114, 'CK882620200325', 2, 1, 280, 1),
(115, 'CK108320200325', 2, 1, 280, 1),
(116, 'CK794320200325', 2, 6, 150, 1),
(117, 'CK794320200325', 2, 7, 280, 1),
(118, 'CK472120200325', 2, 2, 100, 1),
(119, 'CK957920200325', 2, 3, 280, 1),
(120, 'CK957920200325', 2, 3, 150, 1),
(121, 'CK595420200325', 2, 1, 400, 1),
(122, 'CK159620200325', 2, 1, 500, 1),
(123, 'CK736220200325', 2, 1, 55, 1),
(124, 'CK972320200325', 2, 1, 120, 1),
(125, 'CK972320200325', 2, 1, 220, 1),
(126, 'CK972320200325', 2, 1, 100, 1),
(127, 'CK748720200326', 2, 1, 180, 1),
(128, 'CK748720200326', 2, 1, 60, 1),
(129, 'CK748720200326', 2, 1, 150, 1),
(130, 'CK924720200326', 2, 2, 500, 1),
(131, 'CK924720200326', 2, 2, 18, 1),
(132, 'CK804220200326', 2, 1, 24, 1),
(133, 'CK804220200326', 2, 1, 820, 1),
(134, 'CK804220200326', 2, 1, 500, 1),
(135, 'CK012920200326', 2, 3, 280, 1),
(136, 'CK012920200326', 2, 4, 150, 1),
(137, 'CK012920200326', 2, 3, 500, 1),
(138, 'CK012920200326', 2, 3, 400, 1),
(139, 'CK615520200326', 2, 2, 150, 1),
(140, 'CK615520200326', 2, 1, 150, 1),
(141, 'CK615520200326', 2, 1, 24, 1),
(142, 'CK160920200326', 2, 2, 150, 1),
(143, 'CK160920200326', 2, 3, 12, 1),
(144, 'CK493120200326', 2, 2, 150, 1),
(145, 'CK493120200326', 2, 3, 500, 1),
(146, 'CK696320200326', 2, 4, 150, 1),
(147, 'CK000420200326', 2, 2, 10, 1),
(148, 'CK000420200326', 2, 1, 340, 1),
(149, 'CK000420200326', 2, 1, 400, 1),
(150, 'CK000420200326', 2, 1, 170, 1),
(151, 'CK000420200326', 2, 1, 100, 1),
(152, 'CK469620200326', 2, 1, 150, 1),
(153, 'CK369520200326', 2, 2, 100, 1),
(154, 'CK436920200327', 2, 1, 130, 1),
(155, 'CK163720200327', 2, 1, 350, 1),
(156, 'CK163720200327', 2, 1, 340, 1),
(157, 'CK163720200327', 2, 1, 650, 1),
(158, 'CK750820200402', 2, 2, 100, 1),
(159, 'CK257620200402', 2, 1, 60, 1),
(160, 'CK257620200402', 2, 1, 2500, 1),
(161, 'CK257620200402', 2, 2, 150, 1),
(162, 'CK257620200402', 2, 2, 100, 1),
(163, 'CK301220200402', 2, 1, 35, 1),
(164, 'CK210320200402', 2, 4, 100, 1),
(165, 'CK210320200402', 2, 3, 300, 1),
(166, 'CK727020200402', 2, 8, 130, 1),
(167, 'CK202720200402', 2, 1, 100, 1),
(168, 'CK202720200402', 2, 1, 42, 1),
(169, 'CK568620200402', 2, 1, 100, 1),
(170, 'CK420720200403', 2, 1, 24, 1),
(171, 'CK420720200403', 2, 1, 18, 1),
(172, 'CK420720200403', 2, 1, 500, 1),
(173, 'CK420720200403', 2, 1, 820, 1),
(174, 'CK858520200403', 2, 1, 18, 1),
(175, 'CK858520200403', 2, 1, 18, 1),
(176, 'CK479220200403', 2, 3, 150, 1),
(177, 'CK479220200403', 2, 2, 650, 1),
(178, 'CK479220200403', 2, 2, 130, 1),
(179, 'CK479220200403', 2, 1, 150, 1),
(180, 'CK885720200403', 2, 1, 170, 1),
(181, 'CK885720200403', 2, 1, 250, 1),
(182, 'CK868620200403', 2, 1, 18, 1),
(183, 'CK868620200403', 2, 1, 24, 1),
(184, 'CK868620200403', 2, 1, 80, 1),
(185, 'CK868620200403', 2, 1, 500, 1),
(186, 'CK868620200403', 2, 1, 18, 1),
(187, 'CK868620200403', 2, 1, 35, 1),
(188, 'CK398220200404', 2, 1, 500, 1),
(189, 'CK605120200405', 2, 2, 18, 1),
(190, 'CK605120200405', 2, 2, 89, 1),
(191, 'CK542720200405', 2, 1, 18, 1),
(192, 'CK542720200405', 2, 2, 300, 1),
(193, 'CK542720200405', 2, 3, 24, 1),
(194, 'CK410020200405', 2, 1, 0, 1),
(195, 'CK228020200405', 2, 1, 100, 1),
(196, 'CK042120200405', 2, 2, 100, 1),
(197, 'CK851520200406', 2, 2, 18, 1),
(198, 'CK851520200406', 2, 1, 18, 1),
(199, 'CK402420200406', 2, 1, 18, 1),
(200, 'CK473820200407', 2, 1, 150, 1),
(201, 'CK287420200407', 2, 2, 600, 1),
(202, 'CK144020200407', 2, 3, 600, 1),
(203, 'CK918920200408', 2, 2, 50, 1),
(204, 'CK918920200408', 2, 1, 750, 1),
(205, 'CK918920200408', 2, 1, 10, 1),
(206, 'CK918920200408', 2, 1, 50, 1),
(207, 'CK884820200408', 2, 1, 340, 1),
(208, 'CK884820200408', 2, 1, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `product_name` varchar(77) DEFAULT NULL,
  `product_image` varchar(200) DEFAULT NULL,
  `product_price` int(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `category_id`, `product_name`, `product_image`, `product_price`, `status`) VALUES
(1, 61, 'chilli powder', 'assets/uploads/services/1614395261_FB_IMG_1608811785291.jpg', 75, 2),
(2, 61, 'macrons', 'assets/uploads/services/1614405435_macrons.jpeg', 75, 0),
(3, 60, 'ladies finger', 'assets/uploads/services/1614405930_ladiesfinger.jpeg', 90, 1),
(5, 61, 'macrons', 'assets/uploads/services/1614407648_macrons.jpeg', 45, 1),
(6, 62, 'Blueberry', 'assets/uploads/services/1614407743_blueberry.jpeg', 30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `logo` varchar(250) NOT NULL,
  `favicon` varchar(250) NOT NULL,
  `smtp_username` varchar(250) NOT NULL,
  `smtp_host` varchar(250) NOT NULL,
  `smtp_password` varchar(250) NOT NULL,
  `admin_email` varchar(250) NOT NULL,
  `country` varchar(255) NOT NULL,
  `paypal` varchar(250) NOT NULL,
  `paypalid` varchar(250) NOT NULL,
  `serv_secret_key` text NOT NULL,
  `currency` varchar(250) CHARACTER SET utf8 NOT NULL,
  `currency_symbol` varchar(500) NOT NULL,
  `paypal_option` varchar(250) NOT NULL,
  `card_option` varchar(50) NOT NULL COMMENT 'Authorize Or BrainTree',
  `authorize_net_url` varchar(250) NOT NULL,
  `authorize_key` varchar(250) NOT NULL,
  `authorize_id` varchar(250) NOT NULL,
  `social_sign` int(11) NOT NULL,
  `fb_app_id` varchar(100) NOT NULL,
  `fb_sec_key` varchar(100) NOT NULL,
  `google_app_id` varchar(255) NOT NULL,
  `google_sec_key` varchar(100) NOT NULL,
  `payment_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `logo`, `favicon`, `smtp_username`, `smtp_host`, `smtp_password`, `admin_email`, `country`, `paypal`, `paypalid`, `serv_secret_key`, `currency`, `currency_symbol`, `paypal_option`, `card_option`, `authorize_net_url`, `authorize_key`, `authorize_id`, `social_sign`, `fb_app_id`, `fb_sec_key`, `google_app_id`, `google_sec_key`, `payment_type`) VALUES
(1, 'Clickkart', 'http://192.168.138.31/TRAINEES/Asha/original_clickkart1/click_kart/admin/assets/uploads/store_logo/1483423555_cklogo.png', 'http://192.168.138.31/TRAINEES/Asha/original_clickkart1/click_kart/admin/assets/uploads/store_logo/1483420838_cklogo1.png', 'info@techware.in', 'mail.techware.in', 'Golden_123', 'info@techware.in', 'IN', 'https://www.sandbox.paypal.com/cgi-bin/webscr', 'shajeermhmmd@gmail.com', 'My_key', '₹', 'INR', 'Credit_Card,Cash,PayPal', 'BrainTree', 'https://test.authorize.net/gateway/transact.dll', '6Wxf5863CD67gCrh', '5PvGS4m8s', 1, '384079871940470', '66357561d36deca472870c19c2ce96cd', '649072225656-u5cj0tjv2llq2r0fa8g5af783btkts29.apps.googleusercontent.com', 'sBb_rnItMEOaOT29uYf974m-', 0);

-- --------------------------------------------------------

--
-- Table structure for table `store_product`
--

DROP TABLE IF EXISTS `store_product`;
CREATE TABLE IF NOT EXISTS `store_product` (
  `sp_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `delete_status` tinyint(4) NOT NULL COMMENT '0=>inactive 1=>active ',
  `status` tinyint(4) NOT NULL COMMENT '0=>inactive 1=>active',
  PRIMARY KEY (`sp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=213 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_product`
--

INSERT INTO `store_product` (`sp_id`, `store_id`, `product_id`, `delete_status`, `status`) VALUES
(1, 1, 2, 1, 0),
(2, 2, 1, 0, 0),
(3, 1, 3, 1, 0),
(4, 4, 5, 1, 0),
(5, 2, 1, 0, 0),
(6, 12, 5, 1, 1),
(7, 3, 1, 1, 1),
(8, 6, 8, 0, 1),
(9, 11, 10, 1, 0),
(10, 8, 5, 1, 1),
(11, 1, 9, 1, 0),
(12, 2, 2, 1, 0),
(13, 15, 1, 1, 1),
(14, 4, 8, 1, 0),
(15, 1, 2, 1, 0),
(16, 2, 6, 1, 0),
(17, 16, 8, 1, 1),
(18, 13, 5, 0, 0),
(19, 16, 1, 1, 1),
(20, 10, 10, 1, 1),
(21, 5, 9, 1, 0),
(22, 17, 5, 1, 1),
(23, 14, 10, 1, 0),
(24, 7, 10, 1, 1),
(25, 9, 32, 1, 1),
(26, 2, 7, 1, 0),
(27, 16, 1, 1, 0),
(28, 16, 28, 1, 0),
(29, 5, 2, 1, 0),
(30, 5, 32, 1, 0),
(31, 13, 5, 0, 0),
(32, 8, 1, 1, 0),
(33, 17, 6, 1, 1),
(34, 18, 0, 1, 1),
(35, 19, 2, 0, 0),
(36, 19, 30, 0, 0),
(37, 18, 4, 1, 1),
(38, 3, 2, 1, 0),
(40, 18, 7, 1, 1),
(42, 19, 3, 1, 0),
(49, 13, 6, 0, 0),
(50, 19, 9, 1, 0),
(53, 19, 29, 1, 0),
(56, 19, 1, 1, 0),
(59, 19, 5, 0, 0),
(60, 19, 10, 0, 0),
(61, 19, 27, 0, 0),
(62, 1, 48, 0, 0),
(63, 1, 49, 1, 0),
(64, 1, 50, 1, 0),
(65, 3, 51, 1, 1),
(66, 3, 52, 1, 1),
(67, 9, 6, 1, 1),
(68, 2, 53, 1, 0),
(69, 19, 7, 0, 0),
(70, 19, 1, 1, 0),
(71, 20, 0, 0, 0),
(72, 6, 54, 1, 1),
(73, 4, 55, 1, 0),
(74, 5, 56, 1, 0),
(75, 1, 57, 1, 0),
(76, 8, 58, 1, 1),
(77, 7, 59, 0, 1),
(78, 7, 60, 1, 1),
(79, 9, 61, 1, 1),
(80, 5, 62, 1, 0),
(81, 19, 63, 1, 0),
(82, 5, 64, 1, 0),
(83, 2, 65, 1, 0),
(84, 3, 66, 1, 1),
(85, 21, 0, 0, 0),
(86, 6, 67, 1, 1),
(87, 7, 68, 1, 1),
(88, 19, 2, 1, 0),
(89, 13, 35, 1, 0),
(90, 8, 69, 1, 1),
(91, 5, 70, 1, 0),
(92, 5, 71, 1, 0),
(93, 6, 72, 1, 1),
(94, 22, 0, 0, 0),
(95, 7, 73, 1, 1),
(96, 4, 74, 1, 0),
(97, 5, 75, 1, 0),
(98, 7, 76, 1, 1),
(99, 7, 77, 1, 1),
(100, 2, 78, 1, 0),
(101, 1, 79, 1, 0),
(102, 12, 3, 1, 1),
(103, 23, 0, 0, 0),
(104, 4, 80, 1, 0),
(105, 24, 0, 0, 0),
(106, 25, 0, 0, 0),
(107, 25, 81, 1, 0),
(108, 26, 0, 0, 1),
(109, 27, 0, 0, 0),
(110, 28, 0, 0, 1),
(111, 25, 82, 1, 0),
(112, 25, 83, 0, 0),
(113, 28, 84, 1, 1),
(114, 28, 85, 1, 1),
(115, 29, 0, 0, 0),
(116, 29, 86, 1, 0),
(117, 30, 0, 0, 0),
(118, 10, 87, 1, 1),
(119, 10, 88, 1, 1),
(120, 28, 89, 1, 1),
(121, 7, 90, 1, 1),
(122, 7, 91, 1, 1),
(123, 28, 55, 1, 1),
(124, 28, 9, 1, 1),
(125, 28, 3, 1, 1),
(126, 25, 9, 1, 0),
(127, 28, 92, 1, 1),
(128, 28, 93, 1, 1),
(129, 28, 94, 1, 1),
(130, 28, 95, 1, 1),
(131, 28, 6, 1, 1),
(132, 28, 35, 1, 1),
(133, 28, 56, 1, 1),
(134, 31, 0, 0, 1),
(135, 31, 96, 1, 1),
(136, 0, 97, 1, 1),
(137, 31, 98, 0, 1),
(138, 3, 99, 1, 1),
(139, 31, 5, 1, 1),
(140, 26, 100, 1, 1),
(141, 25, 101, 1, 0),
(142, 26, 102, 1, 1),
(143, 25, 103, 1, 0),
(144, 32, 0, 0, 1),
(145, 32, 104, 1, 1),
(146, 26, 105, 1, 1),
(147, 33, 0, 0, 1),
(148, 33, 106, 1, 1),
(149, 34, 0, 0, 1),
(150, 31, 107, 1, 1),
(151, 31, 108, 1, 1),
(152, 28, 109, 1, 1),
(153, 35, 0, 0, 1),
(154, 34, 110, 1, 1),
(155, 34, 111, 1, 1),
(156, 35, 112, 1, 1),
(157, 34, 113, 1, 1),
(158, 35, 114, 1, 1),
(159, 36, 0, 0, 1),
(160, 6, 115, 1, 1),
(161, 35, 116, 1, 1),
(162, 36, 117, 1, 1),
(163, 32, 118, 1, 1),
(164, 33, 119, 1, 1),
(165, 32, 120, 1, 1),
(166, 32, 121, 1, 1),
(167, 28, 122, 1, 1),
(168, 37, 0, 0, 1),
(169, 37, 123, 1, 1),
(170, 37, 124, 1, 1),
(171, 37, 125, 1, 1),
(172, 38, 0, 0, 1),
(173, 38, 126, 1, 1),
(174, 38, 127, 1, 1),
(175, 38, 128, 1, 1),
(176, 36, 8, 1, 1),
(177, 38, 1, 1, 1),
(178, 1, 51, 1, 0),
(179, 1, 129, 1, 0),
(180, 39, 0, 0, 2),
(181, 40, 0, 0, 0),
(182, 41, 0, 0, 2),
(183, 42, 0, 0, 1),
(184, 41, 130, 1, 2),
(185, 41, 51, 1, 2),
(186, 43, 0, 0, 2),
(187, 44, 0, 0, 2),
(188, 44, 131, 1, 2),
(189, 35, 132, 1, 1),
(190, 35, 133, 1, 1),
(191, 35, 134, 1, 1),
(192, 0, 135, 1, 1),
(193, 44, 136, 1, 2),
(194, 1, 58, 1, 0),
(195, 45, 0, 0, 2),
(196, 46, 0, 0, 2),
(197, 47, 0, 0, 2),
(198, 48, 0, 0, 2),
(199, 46, 137, 1, 2),
(200, 45, 138, 1, 2),
(201, 46, 1, 1, 1),
(202, 41, 138, 1, 1),
(203, 46, 137, 1, 1),
(204, 49, 0, 0, 1),
(205, 50, 0, 0, 1),
(206, 51, 0, 0, 1),
(207, 46, 139, 1, 1),
(208, 3, 140, 1, 1),
(209, 0, 141, 1, 1),
(210, 52, 0, 0, 1),
(211, 53, 0, 0, 1),
(212, 54, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
CREATE TABLE IF NOT EXISTS `user_profile` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `phone_no` varchar(12) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `district` varchar(20) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `status` tinyint(3) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=153 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`user_id`, `fullname`, `email`, `phone_no`, `image`, `district`, `city_id`, `status`, `password`) VALUES
(1, 'test', 'test@gmail.com', '123456789', 'assets/uploads/images/IMG1572843458', 'wertfg', 1, 1, '202cb962ac59075b964b07152d234b70'),
(2, 'Freddy', 'freddy@gmail.com', '908543642', 'img_t2345', 'ernakulam', 5, 1, 'e10adc3949ba59abbe56e057f20f883e'),
(3, 'vyshna', 'vyshna@gmail.com', '962973437', 'dbdsnm', 'Kannur', 1, 1, '202cb962ac59075b964b07152d234b70'),
(4, 'vivek ', 'vivek@gmail.com', '962973437', 'jhj', 'Kannur', 1, 1, '202cb962ac59075b964b07152d234b70'),
(5, 'niraj', 'niraj@gmail.com', '805636569', 'img_neeru', 'ernakulam', 5, 1, '81dc9bdb52d04dc20036dbd8313ed055'),
(6, 'vicky', 'vicky@gmail.com', '2147483647', 'img_vicky', 'ernakulam', 10, 0, '8af433519d6e385e89bb280f8002f2b2'),
(7, 'niranjana vivek', 'niranjana@gmail.com', '1234567890', 'img_nira', 'ernakulam', 0, 1, '81dc9bdb52d04dc20036dbd8313ed055'),
(8, 'Asif Ali', 'asifali@gmail.com', '9123456', 'assets/uploads/services/1565585281_so11.PNG', 'Calicut', NULL, 0, NULL),
(34, 'sherin', 'sherin@gmail.com', '9562224719', NULL, 'kollam', 1, 0, 'd73887420763638ccea5d1b1f4f1454c'),
(28, 'Vivek Dhas', 'vivek.sacs@gmail.com', '9629734373', 'assets/uploads/services/1568284598_depositphotos_80441828-stock-illustration-shopkeeper-vector-design.jpg', 'kochi', 1, 0, '202cb962ac59075b964b07152d234b70'),
(31, 'abc', 'abc@gmail.com', '00000000', NULL, 'ernakulam', NULL, 0, '81dc9bdb52d04dc20036dbd8313ed055'),
(27, 'kjhiuk', '484684@gmail.com', '9629734373', 'assets/uploads/services/1568284387_daily_arket.jpg', 'kochi', 3, NULL, '25d55ad283aa400af464c76d713c07ad'),
(29, 'anitt', 'demo2@gmail.com', '7998765431', 'assets/uploads/upload_files/20200123101927136918_EVT_IMG.jpg', 'ernakulam', 5, 1, '25d55ad283aa400af464c76d713c07ad'),
(30, 'abc', 'abc@gmail.com', '123456789', NULL, 'ernakulam', NULL, 0, '81dc9bdb52d04dc20036dbd8313ed055'),
(32, 'abc', 'abc@gmail.com', '000', NULL, 'ernakulam', NULL, 0, '81dc9bdb52d04dc20036dbd8313ed055'),
(33, 'abc', 'abc12@gmail.com', '0000000', 'assets/uploads/images/IMG1574054330', 'ernakulam', NULL, 0, '81dc9bdb52d04dc20036dbd8313ed055'),
(23, 'abc', 'abc@gmail.com', '5086355676', NULL, 'ernakulam', NULL, 0, '81dc9bdb52d04dc20036dbd8313ed055'),
(22, 'abcsd', 'abc@gmail.com', '856230156', NULL, 'ernakulam', NULL, 0, '81dc9bdb52d04dc20036dbd8313ed055'),
(35, 'abc', 'abc121@gmail.com', '13213', NULL, 'ernakulam', 1, 0, '81dc9bdb52d04dc20036dbd8313ed055'),
(36, 'abc', 'a4521@gmail.com', '525555', NULL, 'ernakulam', 1, 0, '81dc9bdb52d04dc20036dbd8313ed055'),
(37, 'abc', 'amal141553@gmail.com', '9565415485', NULL, 'ernakulam', NULL, 0, '25f9e794323b453885f5181f1b624d0b'),
(38, 'abc', 'anitt141553@gmail.com', '2565415485', NULL, 'ernakulam', NULL, 0, '25f9e794323b453885f5181f1b624d0b'),
(39, 'abc', 'anit141553@gmail.com', '1565415485', NULL, 'ernakulam', NULL, 0, '25f9e794323b453885f5181f1b624d0b'),
(40, 'anitt', 'anit14155@gmail.com', '4565415485', NULL, 'ernakulam', NULL, 0, '25f9e794323b453885f5181f1b624d0b'),
(41, 'Test Customer', 'customer@mailinator.com', '1100258963', 'assets/uploads/services/1583297918_Apple.jpg', 'Kochi', 6, 0, '25d55ad283aa400af464c76d713c07ad'),
(42, 'Test Customer', 'test.custom@gmail.com', '1100558899', 'assets/uploads/services/1583299346_Apple.jpg', 'Kochi', 2, 0, '25d55ad283aa400af464c76d713c07ad'),
(43, 'geethu', 'geethu12@gmail.com', '12345678', 'assets/uploads/services/1583401953_12.jpg', 'kochi', 4, 0, '25d55ad283aa400af464c76d713c07ad'),
(44, 'ram', 'ram12@gmail.com', '9667845632', 'assets/uploads/services/1583468230_12.jpg', 'kannur', 8, 0, '25d55ad283aa400af464c76d713c07ad'),
(45, 'hari', 'hari12@gmail.com', '9664523421', 'assets/uploads/services/1583468506_12.jpg', 'kochi', 2, 0, '827ccb0eea8a706c4c34a16891f84e7b'),
(46, 'prav', 'prav@gmail.com', '1234567810', 'assets/uploads/upload_files/20200309101001892925_EVT_IMG.jpg', 'kollam', 1, 0, '25d55ad283aa400af464c76d713c07ad'),
(47, 'f', 'f@gmail.com', '1234567892', 'assets/uploads/upload_files/20200310013058676315_EVT_IMG.jpg', 'kollam', 1, 0, '25d55ad283aa400af464c76d713c07ad'),
(48, 'manju', 'manju12@gmail.com', '9447896543', NULL, 'kannur', 1, 0, '25d55ad283aa400af464c76d713c07ad'),
(49, 'deepu', 'deepu12@gmail.com', '9778890564', 'assets/uploads/services/1583820111_11.jpg', 'kochi', 3, 0, '25d55ad283aa400af464c76d713c07ad'),
(50, 'mikku', 'mikku12@gmail.com', '96675436243', 'assets/uploads/upload_files/20200327043224742591_EVT_IMG.jpg', 'kannur', 2, 1, '25d55ad283aa400af464c76d713c07ad'),
(51, 'Rahul raj', 'rahul12@gmail.com', '9667789875as', 'assets/uploads/upload_files/20200311013015107518_EVT_IMG.jpeg', 'kochi', 1, 1, '25f9e794323b453885f5181f1b624d0b'),
(52, 'tiya', 'tiya12@gmail.com', '9447756432', NULL, 'trivandram', 1, 0, '25d55ad283aa400af464c76d713c07ad'),
(53, 'ichu', 'ichu12@gmail.com', '9557894325', NULL, 'kochi', 5, 0, '25d55ad283aa400af464c76d713c07ad'),
(54, 'job', 'job12@gmail.com', '9556789453', NULL, 'kochi', 5, 0, '25d55ad283aa400af464c76d713c07ad'),
(55, 'lilly', 'lilly12@gmail.com', '9887654325', 'assets/uploads/services/1583914977_11.jpg', 'kochi', 5, 0, '25d55ad283aa400af464c76d713c07ad'),
(56, 'robin', 'robin12@gmail.com', '9887656743', 'assets/uploads/upload_files/20200327032750578657_EVT_IMG.jpg', 'kochi', 1, 1, '25d55ad283aa400af464c76d713c07ad'),
(57, 'rithu', 'rithu12@gmail.com', '9887654786', 'assets/uploads/services/1584097639_margarita-zueva-CY-OkOICA9o-unsplash.jpg', 'kochi', 1, 0, '25d55ad283aa400af464c76d713c07ad'),
(58, 'miya', 'miya12@gmail.com', '9447726284', NULL, 'kochi', NULL, 0, '25f9e794323b453885f5181f1b624d0b'),
(59, 'liya', 'liya12@gmail.com', '9961071274', NULL, 'kochi', NULL, 0, '25d55ad283aa400af464c76d713c07ad'),
(60, 'giya', 'giya12@gmail.com', '9556789345', 'assets/uploads/services/1585100590_13.jpg', 'kannur', 18, 1, '25d55ad283aa400af464c76d713c07ad'),
(61, 'kamal', 'kamal12@gmail.com', '9775678942', 'assets/uploads/services/1585198967_15.jpg', 'kochi', 6, 1, '25d55ad283aa400af464c76d713c07ad'),
(62, 'abc', 'vyshna143@gmail.com', '70109134422', NULL, 'ernakulam', NULL, 2, '25f9e794323b453885f5181f1b624d0b'),
(63, 'abc', 'vyshna143@gmail.com', '70109134422', NULL, 'ernakulam', NULL, 1, '25f9e794323b453885f5181f1b624d0b'),
(64, 'jithu', 'jithu12@gmail.com', '9447726284', NULL, 'kochi', NULL, 2, '25d55ad283aa400af464c76d713c07ad'),
(65, 'mili', 'mili12@gmail.com', '9447936284', NULL, 'kakkanad', 1, 1, '25f9e794323b453885f5181f1b624d0b'),
(66, 'tester', 'tester@mailinator.com', '9497376861', NULL, 'Thrissur', 1, 1, '25d55ad283aa400af464c76d713c07ad'),
(67, 'hari', 'hari14@gmail.com', '9447726285', 'assets/uploads/upload_files/20200327033445832796_EVT_IMG.jpg', 'kannur', 10, 1, '25f9e794323b453885f5181f1b624d0b'),
(68, 'demo', 'demo@gmail.com', '1234567810', NULL, 'ernakulam', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(69, 'demo', 'demo1@gmail.com', '1234569870', NULL, 'ernakulam', NULL, 1, '25f9e794323b453885f5181f1b624d0b'),
(70, 'demo', 'demo1@gmail.com', '1234569870', NULL, 'ernakulam', NULL, 1, '25f9e794323b453885f5181f1b624d0b'),
(71, 'demo', 'demo1@gmail.com', '1234569870', NULL, 'ernakulam', NULL, 1, '25f9e794323b453885f5181f1b624d0b'),
(72, 'demo', 'demo1@gmail.com', '1234569870', NULL, 'ernakulam', NULL, 1, '25f9e794323b453885f5181f1b624d0b'),
(73, 'demo', 'demo2@gmail.com', '123225698701', NULL, 'ernakulam', 1, 1, '123456'),
(87, 'vido Africa', '2vyfrh@gmail.com', '805045008780', NULL, 'Gaduewa', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(74, 'jel', 'jel@gmail.com', '8921184362', NULL, 'ernakulam', 1, 1, '25f9e794323b453885f5181f1b624d0b'),
(75, 'razil', '123razil@gmail.com', '8281013773', NULL, 'malapuram', 1, 1, '25d55ad283aa400af464c76d713c07ad'),
(76, 'Yogesh', 'bhavna_yogi@yahoo.com', '9979496245', NULL, 'Kutch', NULL, 1, '995bf3394c81c556054aad920e0f2513'),
(77, 'demo', 'demo3@gmail.com', '0661234567', 'assets/uploads/upload_files/2020040214213655307_EVT_IMG.jpeg', 'usa', 1, 1, '25d55ad283aa400af464c76d713c07ad'),
(78, 'Test user', 'ikebn412@gmail.com', '0231234567', NULL, 'Gh', 23, 1, '25d55ad283aa400af464c76d713c07ad'),
(79, 'bbbbb', 'bbb@b.com', '1234568523', NULL, 'tttt', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(80, 'shubham', 'skvatsa684@gmail.com', '7053402872', NULL, 'delhi', 1, 1, '25d55ad283aa400af464c76d713c07ad'),
(81, 'marcel', 'marceldennisxp@yahoo.ca', '4167271234', NULL, 'Toronto', 7, 1, '4b9f0ecac4d2e7b86d49c5de20d44c1a'),
(82, 'edgardo', 'asd@gmail.com', '4615784554', NULL, 'bogota', 19, 1, 'a68b3089cfdf237db2c53b83efa1916d'),
(83, 'test', 'nav8621@gmail.com', '8287582607', NULL, 'otwa', NULL, 1, '5d93ceb70e2bf5daa84ec3d0cd2c731a'),
(84, 'nura', 'nurafiles@gmail.com', '0777034414', NULL, 'Colombo', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(85, 'ghh', 'aliadnanhost@gmail.com', '9274188413', NULL, 'abbj', 1, 1, '25d55ad283aa400af464c76d713c07ad'),
(86, 'p d', 'prayag25@gmail.com', '8976667321', NULL, 'thane', 1, 0, '1c1e697916b3e7bfb72a0b1dd28d9433'),
(88, 'aaaaa', 'aaaa@aaaa.com', '12345678911', 'assets/uploads/upload_files/20200403081331137436_EVT_IMG.jpg', 'Kannur', 2, 1, '25d55ad283aa400af464c76d713c07ad'),
(89, 'kleitz', 'ikleitz@gmail.com', '16702588888', NULL, 'rdrr', 1, 1, '25d55ad283aa400af464c76d713c07ad'),
(90, 'afsal', 'meafsal@gmail.com', '9999999999', NULL, 'trissur', 6, 1, '25f9e794323b453885f5181f1b624d0b'),
(91, 'bitoo', 'bitoo@ymail.com', '9905561313', NULL, 'delhi', 1, 1, '25d55ad283aa400af464c76d713c07ad'),
(92, 'imtiaz', 'imtiazanik33@gmail.com', '01611124446', NULL, 'rangamati', NULL, 1, '31aad676cd456e9af8c2914475bc7565'),
(93, 'huy', 'huy@huy.com', '25398545604', NULL, 'hy', NULL, 1, '0aca6dd8b17e6fb51d0747ad492e9a63'),
(94, 'heheyewhyotms', 'heywy@jdjdj.com', '3229143041', NULL, 'hdhsh', NULL, 1, '4c86aa32e8d534d8dd27c9bae3ae45c7'),
(95, 'hshsh', 'jdjdj@fjfj.com', '3229143042', NULL, 'ududu', NULL, 1, '4c86aa32e8d534d8dd27c9bae3ae45c7'),
(96, 'Aleks', 'trojanservice2010@gmail.c', '80293522289', NULL, '???????', NULL, 1, 'd05c9ef15bb6b2f81b4505cd239fe3bc'),
(97, 'rsjhgg', 'thighh@gmail.com', '7010745235', NULL, 'nanbbn', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(98, 'sjjsj', 'test1@gmail.com', '979797997979', NULL, 'nsjs', NULL, 1, '0e7517141fb53f21ee439b355b5a1d0a'),
(99, 'pri', 'xx@yahoo.com', '8335815815', NULL, 'kolkata', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(100, 'demohesap', 'hknceeee@gmail.com', '5387252525', NULL, 'india', 18, 1, 'c28437d82cb5444784fd56450d539939'),
(101, 'Test', 'demodemo@gmail.com', '123457', 'assets/uploads/services/1585955943_favicon.ico', 'ponl', 1, 1, '25d55ad283aa400af464c76d713c07ad'),
(102, 'Mohamed Samy Dridi', 'dridi.medsamy@gmail.com', '0555914536', NULL, 'Annaba', NULL, 1, '82ad7ab784457b308c0868926bffcf7d'),
(103, 'test', 'test@ymail.com', '9996665555', NULL, 'Delhi', NULL, 1, 'ed5fe207a87f5e287c0c38690bfda59f'),
(104, 'shiva ramankatti', 'shivaramankatti28@gmail.c', '9970986889', NULL, 'belgaum', 21, 1, '1adcdeb4fd923d8d081b8d064c190ba7'),
(105, 'prabhakar', 'pavansms750@gmail.com', '9908302604', NULL, 'hyd', NULL, 1, 'd86a381ec447cb4a958d6a9a1a84e922'),
(106, 'achintya', 'rktraders343@gmail.com', '7007165869', NULL, 'Sultanpur', NULL, 1, '85895d282b45604b87cff5a5ab9870e0'),
(107, 'test', 'test@test.com', '9999999990', NULL, 'test', 4, 1, 'test@test.com'),
(113, 'bigbasket', 'bihbasket@gmail.com', '1234568036', NULL, 'felhi', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(108, 'zaland', 'zalandpashton35@gmail.com', '093767341044', NULL, 'paktia', NULL, 1, 'f075fe8b88c4a32f4e68f3ef5edbaeeb'),
(109, 'Vivek Patil', 'ica.vivek16@gmail.com', '9893803615', NULL, 'indore', 1, 1, '25d55ad283aa400af464c76d713c07ad'),
(110, 'aym', 'oneuse2017.7@gmail.com', '01027217288', NULL, 'egypt', 10, 1, 'e69dc2c09e8da6259422d987ccbe95b5'),
(111, 'jdj', 'h@h.com', '65656656565', NULL, 'jdj', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(112, 'peter', 'peterjose473@gmail.com', '1233288935', NULL, 'dk', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(114, 'Thet Kyaw Linn', 'tklinn@gmail.com', '0995498980', NULL, 'yangon', NULL, 1, 'c111f2807223b22e8c407d465b713153'),
(115, 'Kenneth', 'kennethhdc@gmail.com', '50689904783', NULL, 'tabarcia', 1, 1, '1fe78f8339e7e706eb6f5d969db70045'),
(116, 'test', 'kentalmanis021@gmail.com', '089630741800', NULL, 'magelang', NULL, 0, 'c06db68e819be6ec3d26c6038d8e8d1f'),
(117, 'test', 'kentalmanis021@gmail.com', '089630741800', NULL, 'magelang', NULL, 0, 'c06db68e819be6ec3d26c6038d8e8d1f'),
(118, 'test', 'testt@gmail.com', '95073494379', NULL, 'kollam', 7, 1, '25d55ad283aa400af464c76d713c07ad'),
(119, 'nk', 'nareshkfbd@gmail.com', '8368000505', NULL, 'fbd', 23, 1, '25d55ad283aa400af464c76d713c07ad'),
(120, 'Junaid', 'meetjunaidahmad@gmail.com', '0558233076', NULL, 'dubai', NULL, 1, '1ea0744f8834c4d2258b5135e68a4907'),
(121, 'Akil', 'easybaramati@gmail.com', '8888888003', NULL, 'pune', 29, 1, '25d55ad283aa400af464c76d713c07ad'),
(122, 'ali', 'alihamra@gmail.com', '5528536699', NULL, 'Jordan', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(123, 'helder', 'marcelino.eng10@gmail.com', '2389573445', NULL, 'praia', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(124, 'Joao Ribeiro do Teste', 'chriszagatp@gmail.com', '11946431198', NULL, 'Avenida Sao miguel 2', 1, 1, '577e5d45d0aefa6085abab39128c25d8'),
(125, 'mujahid', 'sahifa99@gmail.com', '0541983356', NULL, 'saudi arabia', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(126, 'ftugf', 'admin@gmail.com', '8427109840', NULL, 'mohali', NULL, 1, '75d23af433e0cea4c0e45a56dba18b30'),
(127, 'test', 'mail@mail.com', '5555555555', NULL, 'hgh', 31, 1, '25d55ad283aa400af464c76d713c07ad'),
(128, 'testing customee', 'anjumadan.392@gmail.com', '+19811732542', NULL, 'faridabad', 23, 1, '473cc1f88582836494d1e3e138d62961'),
(129, 'test', 'test1234@gmail.com', '0823564563', NULL, 'test', NULL, 1, '16d7a4fca7442dda3ad93c9a726597e4'),
(130, 'abin', 'abin@gmail.com', '9465849638', NULL, 'delhi', NULL, 1, '421dd7a3fa5516ddae7b1d30ac00b531'),
(131, 'Victor Langula', 'victor.langula@gmail.com', '0624089337', NULL, 'ilalal', 32, 1, '25635a2ac56ffa68a4c582f0f7bd48fd'),
(132, 'jjgfxb', 'sgj@gmail.com', '9098969593', NULL, 'latur', NULL, 1, 'ae4b9071a22ea35e2d81042d3543f0ad'),
(133, 'Vijay Kansal', 'vijay@yopmail.com', '98366546767', NULL, 'test', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(134, 'nevin', 'nevinaugustine@gmail.com', '9035536088', NULL, 'udupi', 1, 1, '6c818f215ca1531d3594f587bd278135'),
(135, 'Mario Benavides Jurado', 'mariobenavidesj@gmail.com', '573173733756', NULL, 'Pasto', NULL, 1, 'd88a556887d209ec02540887d6ff32bc'),
(136, 'junaid', 'gomailbaba@gmail.com', '9875421864', NULL, 'dehli', NULL, 1, 'bc5e8d08ede372e547b400a9bd8be29d'),
(137, 'Akhlesh Singh', 'ask@gmail.com', '6260129453', NULL, 'indore', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(138, 'Akhlesh Singh', 'ram@gmail.com', '9165210808', NULL, 'gwalior', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(139, 'ttttt', 'tttt@tttt.com', '3339098686', NULL, 'mmmm', NULL, 1, '45fcaeafd8ebec14bece68f7f00ca154'),
(140, 'Amjad', 'amjad@amjad.com', '5530879525', NULL, 'Istanbul', 19, 1, '2b6065f912c8d57a9a80e7f9c68b1164'),
(141, 'Kamal Sharma', 'kamaldogra87@gmail.com', '9463662244', NULL, 'Amritsar', NULL, 1, '7266a0eb29edfb12428cd193f0eb6805'),
(142, 'sikandar', 'kwt360@gmail.com', '7054231216', NULL, 'khalilabad', NULL, 1, 'b45d57dffabe642dfc28d2a5611904ee'),
(143, 'sikandar', 'kwt3606@gmail.com', '7054231219', NULL, 'sant', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(144, 'sikandar', 'sikandiir@gmail.com', '8009656946', NULL, 'Khalilabad', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(145, 'Mohamed Elsayed', 'mohamed.elsayed3012@gmail', '971552979914', NULL, 'Abu Dhabi', NULL, 1, '4a1a9bad8f1bc0b2a056a5e60e3e2251'),
(146, 'mm', 'm@gmail.com', '0562915299', NULL, 'dubai', 31, 1, '25d55ad283aa400af464c76d713c07ad'),
(147, 'ffff', 'x@gmail.com', '8523690741', NULL, 'west', 35, 1, '25d55ad283aa400af464c76d713c07ad'),
(148, 'daniel', 'danielfernandescardoso122', '77981523038', NULL, 'guanambi', NULL, 1, 'dc51ef66afa08d47ad85b51fd32a8509'),
(149, 'b', 'mmr.rnga@gmail.com', '01205540082', NULL, 'vb', NULL, 1, '51824f3a0482fa324e049d54117379c2'),
(150, 'antonio cespedes', 'a.cespedes33@gmail.com', '8343042925', NULL, 'Victoria', NULL, 1, '25d55ad283aa400af464c76d713c07ad'),
(151, 'manjunatha', 'ecoinshopee@gmail.com', '9663773362', NULL, 'kolar', NULL, 1, 'bc1025b78be4dc2f127f97d8ab3d3703'),
(152, 'and', 'ann@gmail.com', '7657970242', NULL, 'Pune', NULL, 1, '8d545080d7315901885912db0f0ef2e7');

-- --------------------------------------------------------

--
-- Table structure for table `user_promo`
--

DROP TABLE IF EXISTS `user_promo`;
CREATE TABLE IF NOT EXISTS `user_promo` (
  `up_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `promo_id` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`up_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_promo`
--

INSERT INTO `user_promo` (`up_id`, `user_id`, `promo_id`, `date`, `status`) VALUES
(1, 1, 1, '2019-07-10 18:30:00', 1),
(2, 1, 2, '2019-07-08 18:30:00', 1),
(13, 1, 4, '2019-11-05 00:00:00', 1),
(12, 1, 5, '2019-11-05 00:00:00', 1),
(11, 1, 12, '2019-07-16 18:30:00', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
