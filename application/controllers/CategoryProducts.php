<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryProducts extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kolkata");
        $this->load->model('CategoryProducts_model');
        
    }

    public function index($cat_id=''){
       $cat_id = decode_param($cat_id);
      // print_r($cat_id);exit;
        $template['page'] = 'products';
        $template['productData'] = $this->CategoryProducts_model->get_categoryProducts($cat_id);
        $template['category'] = $this->CategoryProducts_model->get_category();
        $this->load->view('products', $template);
    }

    public function listCart($prd_id=''){ 
        $prd_id1=decode_param($prd_id);
        $template['page'] = 'Cart';
        $template['prdData'] = $this->CategoryProducts_model->get_productData($prd_id1);
        $this->load->view('cart', $template);
    }

    public function checkOut($prd_id=''){
        $prd_id1=decode_param($prd_id);
        $template['page'] = 'Checkout';
        $template['productData'] = $this->CategoryProducts_model->get_productData($prd_id1);
        $this->load->view('checkout', $template);  
    }

    public function placeOrder($prd_id=''){
        
          $flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');

          $prd_id = decode_param($prd_id);

          $placeOrder = $this ->CategoryProducts_model->placeOrder($prd_id);
        
          if($placeOrder==1){

            $flashMsg =array('message'=>'Order Placed..!','class'=>'success');
            $this->session->set_flashdata('message', $flashMsg);

            redirect(base_url('CategoryProducts/index'));
         }
        redirect(base_url(''));
    }

}
?>