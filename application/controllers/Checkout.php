<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kolkata");
        $this->load->model('Checkout_model');
        
    }

    public function checkoutPage(){
        $template['page'] = 'Checkout';
        $template['categoryData'] = $this->Checkout_model->checkout();
        $this->load->view('checkout', $template);
    }

}
?>