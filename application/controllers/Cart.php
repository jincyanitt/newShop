<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kolkata");
        $this->load->model('Cart_model');
        
    }

    public function listCart($prd_id=''){
      
        $prd_id1=decode_param($prd_id);
        //print_r($prd_id1);exit;
        $template['page'] = 'Cart';
       $template['prdData'] = $this->Cart_model->get_CartList();
        $this->load->view('cart', $template);
    }

}
?>