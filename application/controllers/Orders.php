<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Kolkata");
        $this->load->model('Order_model');
        if(!$this->session->userdata('logged_in')) {
			redirect(base_url('Login'));
		}
        
     }
     public function listOrders(){
        $template['page'] = 'Orders/orders_list';
        $template['pTitle'] = "View Orders";
        $template['pDescription'] = "View and Manage Orders"; 
        $template['menu'] = "Order Management";
        $template['smenu'] = "View Orders";
        $template['orderData'] = $this->Order_model->getOrders();

        $this->load->view('template',$template);
    }

    public function getOrderData(){
        $return_arr = array('status'=>'0');
        if(!isset($_POST) || empty($_POST) || !isset($_POST['order_id']) || empty($_POST['order_id']) || empty(decode_param($_POST['order_id']))){
            echo json_encode($return_arr);exit;
        }
        $order_id = decode_param($_POST['order_id']);
        $return_arr['order_data'] = $this->Order_model->getOrderDetails($order_id);
        $return_arr['product_image'] = $this->Order_model->getProductImage($order_id);
        if(!empty($return_arr)){
            $return_arr['status'] = 1;
            echo json_encode($return_arr);exit;
        }
        echo json_encode($return_arr);exit;
    }

    public function changeOrderStatus($order_id = '',$status = ''){
        $flashMsg = array('message'=>'Something went wrong, please try again..!','class'=>'error');
        if(!isset($_POST) || empty($_POST) || !isset($_POST['order_id']) || empty($_POST['order_id']) || empty(decode_param($_POST['order_id']))){
            $this->session->set_flashdata('message',$flashMsg);
        }
        $order_id1=decode_param($order_id);

        $status1 = $this->Order_model->changeOrderStatus($order_id1,$status);
        if(($status==1 || $status==3) && $status1){
            $flashMsg =array('message'=>'Order Accepted..!','class'=>'success');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Orders/listOrders'));
        }
        if($status==2 && $status1){
            $flashMsg =array('message'=>'Order Rejected..!','class'=>'success');
            $this->session->set_flashdata('message', $flashMsg);
            redirect(base_url('Orders/listOrders'));
        }
        redirect(base_url('Orders/listOrders'));
    }

}
?>
