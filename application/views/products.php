<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet"  href="<?= base_url('assets/css/style.css') ?>">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <title>Stones Product</title>
</head>

<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="menu-section">
                        <input type="checkbox" id="check">
                        <label for="check" class="navbar__hamburger-btn">
                            <i class="fas fa-bars"></i>
                        </label>
                        <ul class="d-lg-flex justify-content-between menu">
                            <li>
                                <a href="./index.html">HOME</a>
                            </li>
                            <li>
                                <a href="./about.html">ABOUT</a>
                            </li>
                            <li>
                                <a href="./products.html" class="active">PRODUCTS</a>
                            </li>
                            <li>
                                <a href="#">FAQ</a>
                            </li>
                            <li>
                                <a href="#">BLOG</a>
                            </li>
                            <li>
                                <a href="#">CONTACT</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-banner">
            <div class="add-ons-wrapper">
                <div class="container">
                    <div class="d-md-flex justify-content-between pl-md-5 pr-md-5">
                        <div class="d-flex align-items-center">
                            <img src="<?= base_url('assets/images/common/time.svg') ?>" alt="timely delivery">
                            
                            <span>48hr delivery</span>
                        </div>

                        <div class="d-flex align-items-center">
                            <img src="./assets/images/common/price.svg" alt="Price match Gaurantee">
                            <span>Price match Gaurantee</span>
                        </div>

                        <div class="d-flex align-items-center">
                            <img src="./assets/images/common/split.svg" alt="Split packs">
                            <span>Split packs</span>
                        </div>

                        <div class="d-flex align-items-center">
                            <img src="./assets/images/common/delivery.svg" alt="Free delivery">
                            <span>Free delivery</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main id="products-page">
        <section>
            <div class="product-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="product-left-filter">
                                <div class="accordion" id="products-list">
                                 
                                  
                                    <div class="card">
                                        <div class="card-header" id="headingThree">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" href="#collapsefour" aria-expanded="false" aria-controls="collapseThree">
                                                    Product Categories
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#products-list">
                                            <div class="card-body">

                                            <?php if(isset($category)){
                                                foreach($category as $cat){ ?>    
                                                <a class="nav-link active"  href="<?= base_url("CategoryProducts/index/".encode_param($cat->category_id))?>" role="tab" aria-controls="procelain" aria-selected="true"><?= $cat->category_name ?></a>
                                            <?php } }?>
                                            </div>
                                        </div>
                                    </div>
                            
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="product-details">
                            <?php if(isset($productData)){
                            foreach($productData as $prd){ ?> 
                                <div class="tab-content" id="all-productsContent">
                                    <div class="tab-pane fade show active" id="procelain" role="tabpanel" aria-labelledby="procelain-tab">
                                        <div class="row mb-md-4">
                                            <div class="col-md-4">
                                            <div list-box ="<?php echo $prd->product_id?>"?>
                                                <div class="product-detail-inner">
                                                <img src="<?= base_url($prd->product_image) ?>" listimage ="<?= base_url($prd->product_image) ?>" alt="">
                                                    <div class="product-content">
                                                        <h3 list-name = "<?php echo $prd->product_name?>"?><?= $prd->product_name ?></h3>
                                                        <label class="price d-block">$ <?= $prd->product_price ?></label>
                                                        <button id="contact_details">
                                                        <a href="<?= base_url("CategoryProducts/listCart/".encode_param($prd->product_id))?>">ADD TO CART</a>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                       
                                        </div>
                                           
                                    </div>
                                    <div class="tab-pane fade" id="hand-split-stone" role="tabpanel" aria-labelledby="hand-split-stone-tab">
                                        <div class="row">
                                        
                                        </div>
                                    </div>
                                </div>
                                <?php } }?> 
                            </div>
                            <div class="pagination-wrapper d-flex align-items-center justify-content-center mt-5">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <li class="page-item"><a class="page-link" href="#!">Previous</a></li>
                                        <li class="page-item "><a class="page-link active" href="#!">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#!">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#!">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#!">4</a></li>
                                        <li class="page-item"><a class="page-link" href="#!">5</a></li>
                                        <li class="page-item"><a class="page-link" href="#!">6</a></li>
                                        <li class="page-item"><a class="page-link" href="#!">7</a></li>
                                        <li class="page-item"><a class="page-link" href="#!">8</a></li>
                                        <li class="page-item active"><a class="page-link active" href="#!">Next</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer>
        <div class="container">
            <div class="d-md-flex justify-content-between align-items-center">
                <h5>Copyright © 2021.</h5>
                <ul class="d-md-flex ">
                    <li class="mr-md-3"><a href="#">Privacy Policy</a> </li>
                    <li><a href="#">Return Policy</a></li>
                </ul>
            </div>
        </div>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js " integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49 " crossorigin="anonymous "></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js " integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy " crossorigin="anonymous "></script>
    <script src="https://kit.fontawesome.com/f28276eb4f.js" crossorigin="anonymous"></script>
</body>

</html>

<script>

var selected_product_id=0;

$('#contact_details').click(function(){
   // selected_product_id = $(this).closest('div.list-box').attr('data-product');
    var selected_product_id = $(this).closest("div[list-box]").attr('list-box');
    var product_image = $(this).closest("div[list-image]").attr('list-image');
    var product_price = $(this).closest("div[list-price]").attr('list-price');
    var product_name =$(this).closest("div[list-name]").attr('list-name');
    alert(product_name);
    var url ="<?php echo base_url('CategoryProducts/addToCart')?>";
    if(selected_product_id == 0){
        alert("Please choose a product to proceed")
    }else{
        $.ajax({
            type: "POST",
            url: url,
            data: {selected_product_id:selected_product_id},
            success: function(data)
            {
                window.location.replace('<?php echo base_url('Cart/listCart')?>');
            }
        })
           
            
    }
    
})
</script>