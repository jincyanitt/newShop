<?php
  $userData = $this->session->userdata['user'];
?>
<aside class="main-sidebar">
    <section class="sidebar" >
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=base_url($userData->profile_image)?>" onerror="this.src='<?=base_url("assets/images/user_avatar.jpg")?>'" class="user-image left-sid" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('logged_in_admin')['username']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li><a  href="<?= base_url('Dashboard') ?>"><i class="fa fa-wrench" aria-hidden="true">
                </i><span>Dashboard</span></a>
            </li>
                <li class="treeview">
                <a href="<?= base_url('Orders/listOrders') ?>">
                        <i class="fa fa-book" aria-hidden="true"></i>
                        
                            <span>Order Management</span>
                    </a>
                   
                    <?php if($this->session->userdata['user_type'] == 1){ ?>
                    <li class="treeview">
                <a href="#">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                        <span>Category</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?= base_url('Category/addcategory') ?>">
                            <i class="fa fa-circle-o text-aqua"></i>
                            Add New Category
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url('Category/listcategory') ?>">
                            <i class="fa fa-circle-o text-aqua"></i>
                            View All Categories
                        </a>
                    </li>
                    
                </ul>
                <?php } ?>
            </li>
         
            
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                        <span>Product Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?= base_url('Product/addProduct') ?>">
                            <i class="fa fa-circle-o text-aqua"></i>
                            Add New Product
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url('Product/viewProducts') ?>">
                            <i class="fa fa-circle-o text-aqua"></i>
                            View All Product
                        </a>
                    </li>
                </ul>
            </li>
    
            </li></ul>
            
    </section>
</aside>

