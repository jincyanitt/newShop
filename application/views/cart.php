<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <title>Cart - Stones</title>
</head>

<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="menu-section">
                        <input type="checkbox" id="check">
                        <label for="check" class="navbar__hamburger-btn">
                            <i class="fas fa-bars"></i>
                        </label>
                        <ul class="d-lg-flex justify-content-between menu">
                            <li>
                                <a href="./index.html">HOME</a>
                            </li>
                            <li>
                                <a href="./about.html">ABOUT</a>
                            </li>
                            <li>
                                <a href="./products.html" class="active">PRODUCTS</a>
                            </li>
                            <li>
                                <a href="#">FAQ</a>
                            </li>
                            <li>
                                <a href="#">BLOG</a>
                            </li>
                            <li>
                                <a href="#">CONTACT</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-banner">
            <div class="add-ons-wrapper">
                <div class="container">
                    <div class="d-md-flex justify-content-between pl-md-5 pr-md-5">
                        <div class="d-flex align-items-center">
                            <img src="./assets/images/common/time.svg" alt="timely delivery">
                            <span>48hr delivery</span>
                        </div>

                        <div class="d-flex align-items-center">
                            <img src="./assets/images/common/price.svg" alt="Price match Gaurantee">
                            <span>Price match Gaurantee</span>
                        </div>

                        <div class="d-flex align-items-center">
                            <img src="./assets/images/common/split.svg" alt="Split packs">
                            <span>Split packs</span>
                        </div>

                        <div class="d-flex align-items-center">
                            <img src="./assets/images/common/delivery.svg" alt="Free delivery">
                            <span>Free delivery</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main id="cart">
        <section>
            <div class="product-list">
                <div class="container">
                    <h1>My Cart</h1>
                </div>
                <div class="table-list">
                <?php if(isset($prdData)){
                                    foreach($prdData as $prd){ ?>
                    <div class="container">
                        <div class="row"> 
                            <div class="col-md-8">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Product Details</th>
                                            <th scope="col" class="text-center">Price</th>
                                            <th scope="col" class="text-center">Total</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   
                                        <tr>
                                            <th scope="row">
                                                <div class="product-detail-list d-flex" list-box ="<?php echo $prd->product_id?>"?>
                                                    <div>
                                                        <img src="<?= base_url($prd->product_image) ?>" alt="">
                                                    </div>
                                                    <div class="ml-3">
                                                        <h3><?= $prd->product_name?></h3>
                                                        
                                                    </div>
                                                </div>
                                            </th>
                                            <td class="text-center">£<?= $prd->product_price ?></td>
                                            <td class="text-center">£<?= $prd->product_price ?></td>
                                            <td>
                                                <a href="#!">
                                                    <img src="./assets/images/common/trash.svg" alt="">
                                                </a>

                                            </td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                           
                            <div class="col-md-4">
                                <div class="product-total">
                                    <h2>Cart Summary</h2>
                                    <hr>
                                    <div class="d-flex justify-content-between mx-3">
                                        <h5>Sub Total</h5>
                                        <span>£<?= $prd->product_price ?></span>
                                    </div>
                                    <hr>
                                   
                                    <div class="d-flex justify-content-between mt-2">
                                        <button class="cont-shop-btn" >
                                        <a href="<?= base_url("CategoryProducts/index")?>">Keep Shopping</a>
                                        </button>

                                        <button class="stone-btn">
                                        <a href="<?= base_url("CategoryProducts/checkOut/".encode_param($prd->product_id))?>">Check out</a>

                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <?php } }?> 
                </div>
            </div>
        </section>
    </main>
    <footer>
        <div class="container">
            <div class="d-md-flex justify-content-between align-items-center">
                <h5>Copyright © 2020.</h5>
                <ul class="d-md-flex ">
                    <li class="mr-md-3"><a href="#">Privacy Policy</a> </li>
                    <li><a href="#">Return Policy</a></li>
                </ul>
            </div>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js " integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo " crossorigin="anonymous "></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js " integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49 " crossorigin="anonymous "></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js " integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy " crossorigin="anonymous "></script>
</body>

</html>

