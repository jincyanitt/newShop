<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet"  href="<?= base_url('assets/css/style.css') ?>">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <title>Cart - Checkout</title>
</head>

<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="menu-section">
                        <input type="checkbox" id="check">
                        <label for="check" class="navbar__hamburger-btn">
                            <i class="fas fa-bars"></i>
                        </label>
                        <ul class="d-lg-flex justify-content-between menu">
                            <li>
                                <a href="#!">HOME</a>
                            </li>
                            <li>
                                <a href="#!">ABOUT</a>
                            </li>
                            <li>
                                <a href="./products.html" class="active">PRODUCTS</a>
                            </li>
                            <li>
                                <a href="#">FAQ</a>
                            </li>
                            <li>
                                <a href="#">BLOG</a>
                            </li>
                            <li>
                                <a href="#">CONTACT</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-banner">
            <div class="add-ons-wrapper">
                <div class="container">
                    <div class="d-md-flex justify-content-between pl-md-5 pr-md-5">
                        <div class="d-flex align-items-center">
                            <img src="./assets/images/common/time.svg" alt="timely delivery">
                            <span>48hr delivery</span>
                        </div>
                        <div class="d-flex align-items-center">
                            <img src="./assets/images/common/price.svg" alt="Price match Gaurantee">
                            <span>Price match Gaurantee</span>
                        </div>
                        <div class="d-flex align-items-center">
                            <img src="./assets/images/common/split.svg" alt="Split packs">
                            <span>Split packs</span>
                        </div>

                        <div class="d-flex align-items-center">
                            <img src="./assets/images/common/delivery.svg" alt="Free delivery">
                            <span>Free delivery</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main id="checkout">
        <div class="checkout-container">
            <div class="container">
                <h1 class="ml-0">Checkout Page</h1>
                <?php if(isset($productData)){
                            foreach($productData as $prd){ ?> 
                <div class="row">
                    <div class="col-md-8 pl-md-0">
                        <div class="table-list">
                            <div class="container">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Product Details</th>
                                            <th scope="col" class="text-center">Price</th>
                                            <th scope="col" class="text-center">Total</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row" data-label="Product Details">
                                                <div class="product-detail-list d-flex">
                                                    <div>
                                                        <img src="<?= base_url($prd->product_image) ?>" alt="">
                                                    </div>
                                                    <div class="ml-3">
                                                        <h3><?= $prd->product_name ?> </h3>
                                                       
                                                    </div>
                                                </div>
                                            </th>
                                            <td class="text-center" data-label="Price">$ <?= $prd->product_price ?></td>
                                            <td class="text-center" data-label="Total">$ <?= $prd->product_price ?></td>
                                            <td>
                                                <a href="#!">
                                                    <img src="./assets/images/common/trash.svg" alt="">
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkout-wrapper">
                            <div class="payment-wrapper">
                                <div class="row mb-4">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <h6 class="d-flex justify-content-between align-items-center">
                                            <div>
                                                Billing Address
                                            </div>
                                            <a href="#!" class="btn btn-sm btn-link d-block p-0">
                                                Change Address
                                            </a>
                                        </h6>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12 text-left mt-md-3">
                                        <p>John Doe</p>
                                        <p>Address Line 1</p>
                                        <p> Address Line 2</p>
                                        <p>City, london - ABCDEF </p>
                                        <p>+91-9876543210</p>
                                    </div>
                                </div>
                              
                                <div class="row mb-4">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <h6 class="mb-3">Payment Method</h6>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12 cash-delivery">
                                        <select class="form-control" id="payment-method">
                                            <option value="1">Cash On Delivery (COD)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <h6 class="d-flex justify-content-between align-items-center">
                                            <div>Order Summary</div>
                                            <div>
                                                <button class="btn btn-link tax-breakup-toggler p-0">Show Tax Breakup</button>
                                            </div>
                                        </h6>
                                    </div>
                                </div>
                                <div class="row mb-3 mt-3 subtotal-wrapper" id="subtotal-w-o-tax">
                                    <div class="col-sm-6 col-md-6 col-lg-6 col-6">
                                        <p class="mb-0">Subtotal</p>
                                        <p class="mb-0">Delivery</p>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6 col-6 text-right">
                                        <div class="amount subtotal-wrapper">
                                            <p class="mb-0 font-weight-bold">$ <?= $prd->product_price ?></p>
                                            <p class="mb-0 font-weight-bold">$ <?= $prd->product_price ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3 mt-3 subtotal-wrapper" id="subtotal-w-tax" style="display: none;">
                                    <div class="col-sm-6 col-md-6 col-lg-6 col-6 subtotal-wrapper">
                                        <p class="mb-0">Subtotal</p>
                                       
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6 col-6 text-right subtotal-wrapper">
                                        <div class="amount">
                                            <p class="mb-0 font-weight-bold">$ <?= $prd->product_price ?></p>
                                            
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="total-wrapper subtotal-wrapper">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6 col-lg-6 text-left">
                                            <h5>Order Total</h5>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 text-right">
                                            <h5>$ <?= $prd->product_price ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                        <button id="place-order-button" class="stone-btn w-100">
                                        <a href="<?= base_url("CategoryProducts/placeOrder/".encode_param($prd->product_id))?>"> Place Order </a>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } }?> 
            </div>
        </div>
        </section>
    </main>
    <footer>
        <div class="container">
            <div class="d-md-flex justify-content-between align-items-center">
                <h5>QPD stones Copyright © 2020.</h5>
                <ul class="d-md-flex ">
                    <li class="mr-md-3"><a href="#">Privacy Policy</a> </li>
                    <li><a href="#">Return Policy</a></li>
                </ul>
            </div>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js " integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo " crossorigin="anonymous "></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js " integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49 " crossorigin="anonymous "></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js " integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy " crossorigin="anonymous "></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/js/all.min.js" integrity="sha512-LW9+kKj/cBGHqnI4ok24dUWNR/e8sUD8RLzak1mNw5Ja2JYCmTXJTF5VpgFSw+VoBfpMvPScCo2DnKTIUjrzYw==" crossorigin="anonymous"></script>
    <script>
        $('.tax-breakup-toggler').click(function() {
            $(this).text(function(i, text) {
                return text === "Show Tax Breakup" ? "Hide Tax Breakup" : "Show Tax Breakup";
            })
            $('#subtotal-w-tax').toggle();
            $('#subtotal-w-o-tax').toggle();
        });
    </script>
</body>

</html>