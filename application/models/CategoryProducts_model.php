<?php 
class CategoryProducts_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
	}

    public function get_categoryProducts($cat_id=''){

		if($cat_id){

			$res=$this->db->query("SELECT * from products where status=1 and category_id = '".$cat_id."'");
			$result=$res->result();
			return $result;

		}
		$res=$this->db->query("SELECT * from products where status=1");
		$result=$res->result();
		return $result;

    }

	public function get_category(){

		$result = $this->db->query("SELECT * from categories where status =1 Group by category_id");

		$res = $result->result();
		return $res;
	}

	public function get_productData($prd_id=''){

		$result = $this->db->query("SELECT * from products where status =1 and product_id = '".$prd_id."'");

		$res = $result->result();


		return $res;

	}

	public function placeOrder($prd_id=''){

        $result = $this->db->query("SELECT * from products where status =1 and product_id = '".$prd_id."'");

		$prd = $result->row();

		$prd_data['status']=1;

        $prd_data['booking_id']='SH'. date('Ymd');

		$prd_data['product_id'] =$prd->product_id;

		$prd_data['category_id']=$prd->category_id;

		$prd_data['total_amount']= $prd->product_price;

		$prd_data['payment_method'] =1;

		$prd_data['payment_status']=1;

		$status = $this->db->insert('orders',$prd_data);

		return ($status)?1:0;;
	}
}
?>