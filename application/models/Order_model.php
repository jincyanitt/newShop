<?php 
class Order_model extends CI_Model {
  
  public function _consruct(){
    parent::_construct();
  }
  
  public function getOrders($view_all=''){
    //print_r($shopper_id);exit;
    
      $this->db->select("ORD.*,PRD.product_name");
      $this->db->from('orders AS ORD');
      $this->db->join('products AS PRD','PRD.product_id = ORD.product_id');
      $result = $this->db->get()->result();

     // print_r($result);exit;
      if(!empty($result)){
        return $result;
      }
    
    
  }
  
  public function getOrderDetails($order_id){
    if($order_id == ''){
      return 0;
    }
    $result = $this->db->query("SELECT ORD.*,PRD.product_name,CUST.fullname
    FROM orders AS ORD 
    JOIN products AS PRD on PRD.product_id = ORD.product_id 
    JOIN user_profile AS CUST on CUST.user_id = ORD.user_id 
    WHERE ORD.order_id = $order_id");
//print_r($this->db->last_query());exit;
    if(empty($result)){
      return;
    }
    return (empty($order_id))?$result->result():$result->row();
  }
  
  public function getProductImage($order_id){
    $result = $this->db->query("SELECT PRDI.product_image FROM orders AS ORD join products AS PRDI on PRDI.product_id = ORD.product_id WHERE ORD.order_id = $order_id");
    
    return (empty($result))?'':$result->result();
  }
  
  public function changeOrderStatus($order_id,$status=''){

    //print_r($status);exit;
    if(empty($order_id) || $status  == ''){
      return 0;
    }
    
    if($this->db->query("UPDATE orders set `status`=$status WHERE order_id='".$order_id."' ")){
      return 1;
    }
    return 0;
  }
  
}
?>